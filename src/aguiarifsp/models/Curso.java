//******************************************************
//Instituto Federal de São Paulo - Campus Sertãozinho
//Disciplina......: M3LPBD
//Programação de Computadores e Dispositivos Móveis
//Aluno...........: Thiago Vieira de Aguiar
//******************************************************
package aguiarifsp.models;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author aguiar
 */
public class Curso {
    private int id;
    private String nome;

    public Curso(int id, String nome) {
        this.id = id;
        this.nome = nome;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }
   
}
