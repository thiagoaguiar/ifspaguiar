//******************************************************
//Instituto Federal de São Paulo - Campus Sertãozinho
//Disciplina......: M3LPBD
//Programação de Computadores e Dispositivos Móveis
//Aluno...........: Thiago Vieira de Aguiar
//******************************************************
package aguiarifsp.models;

/**
 *
 * @author aguiar
 */
public class Disciplina {
    private int id;
    private String nome;
    private int cargaHoraria;
    private int vagas;
    private Curso curso;
    private Turno turno;

    public Disciplina(int id, String nome, int cargaHoraria, int vagas, Curso curso, Turno turno) {
        this.id = id;
        this.nome = nome;
        this.cargaHoraria = cargaHoraria;
        this.vagas = vagas;
        this.curso = curso;
        this.turno = turno;
    }

    public Disciplina(String nome, int cargaHoraria, int vagas, Curso curso, Turno turno) {
        this.nome = nome;
        this.cargaHoraria = cargaHoraria;
        this.vagas = vagas;
        this.curso = curso;
        this.turno = turno;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public int getCargaHoraria() {
        return cargaHoraria;
    }

    public void setCargaHoraria(int cargaHoraria) {
        this.cargaHoraria = cargaHoraria;
    }

    public int getVagas() {
        return vagas;
    }

    public void setVagas(int vagas) {
        this.vagas = vagas;
    }

    public Curso getCurso() {
        return curso;
    }

    public void setCurso(Curso curso) {
        this.curso = curso;
    }

    public Turno getTurno() {
        return turno;
    }

    public void setTurno(Turno turno) {
        this.turno = turno;
    }

    @Override
    public String toString() {
        return "Disciplina:" + 
                "\n#\t" + id + 
                "\nNome: \t" + nome + 
                "\nCargaHoraria: \t" + cargaHoraria + 
                "\nVagas: \t" + vagas + 
                "\nCurso: \t" + curso.getNome() + 
                "\nTurno: \t" + turno.getNome();
    }
    
    
    
}
