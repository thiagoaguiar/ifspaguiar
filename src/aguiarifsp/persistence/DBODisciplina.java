//******************************************************
//Instituto Federal de São Paulo - Campus Sertãozinho
//Disciplina......: M3LPBD
//Programação de Computadores e Dispositivos Móveis
//Aluno...........: Thiago Vieira de Aguiar
//******************************************************
package aguiarifsp.persistence;

import aguiarifsp.models.Curso;
import aguiarifsp.models.Disciplina;
import aguiarifsp.models.Disciplina;
import aguiarifsp.models.Turno;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author aguiar
 */
public class DBODisciplina {
    
    private Connection dboConnect;
    
    public DBODisciplina() {
        this.dboConnect = DBOConnect.getInstance().getConnection();
    }
    
    /**
     * Seleciona um elemento pelo ID.
     * @param id
     * @return 
     */    
    public Disciplina selectWhereId(int id){
        try {
            Disciplina disciplina = null;
            String statement = "SELECT * FROM disciplinas WHERE "
                    + "id = "+ id 
                    ;
            ResultSet resultSet = this.dboConnect.createStatement().executeQuery(statement);
            if (resultSet.next()){
                disciplina = new Disciplina(
                        resultSet.getInt("id"),
                        resultSet.getString("nome"),
                        resultSet.getInt("cargahoraria"),
                        resultSet.getInt("vagas"),
                        new DBOCurso().selectWhereId(resultSet.getInt("cursos_id")),
                        new DBOTurno().selectWhereId(resultSet.getInt("turnos_id"))
                );
                return disciplina;
            } else {
                return null;
            }
        } catch (SQLException ex) {
            Logger.getLogger(DBODisciplina.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }
   
    /**
     * Seleciona um elemento pelos seus campos.
     * @param nome
     * @param cargaHoraria
     * @param vagas
     * @param cursoId
     * @param turnoId
     * @return 
     */
    public Disciplina selectAND(String nome, int cargaHoraria, int vagas, int cursoId, int turnoId){
        try {
            Disciplina disciplina = null;
            String statement = "SELECT * FROM disciplinas WHERE "
                    + "nome = '" + nome + "'" + " AND "
                    + "cargahoraria = "+ cargaHoraria + " AND "
                    + "vagas = "+ vagas + " AND "
                    + "cursos_id = "+ cursoId + " AND "
                    + "turnos_id = "+ turnoId
                    ;
            ResultSet resultSet = this.dboConnect.createStatement().executeQuery(statement);
            if (resultSet.next()){
                disciplina = new Disciplina(
                        resultSet.getInt("id"),
                        resultSet.getString("nome"),
                        resultSet.getInt("cargahoraria"),
                        resultSet.getInt("vagas"),
                        new DBOCurso().selectWhereId(resultSet.getInt("cursos_id")),
                        new DBOTurno().selectWhereId(resultSet.getInt("turnos_id"))
                );
                return disciplina;
            } else {
                return null;
            }
        } catch (SQLException ex) {
            Logger.getLogger(DBODisciplina.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }
      
    /**
     * Insere um elemento.
     * @param nome
     * @param cargaHoraria
     * @param vagas
     * @param nomeCurso
     * @param nomeDisciplina
     * @return 
     */
    public String insert(String nome, int cargaHoraria, int vagas, String nomeCurso, String nomeDisciplina){
        
        try {
            Curso curso = new DBOCurso().selectWhereString("nome", nomeCurso);
            if (curso == null)
                return "Curso inválido";
            
            Turno turno = new DBOTurno().selectWhereString("nome", nomeDisciplina);
            if (turno == null)
                return "Disciplina inválido";
            
            String statement = "INSERT INTO disciplinas (nome, cargahoraria, vagas, cursos_id, turnos_id) VALUES "
                    + "("
                    + "'" + nome + "'" + ","
                    + cargaHoraria + ","
                    + vagas + ","
                    + curso.getId() + ","
                    + turno.getId() + ")"
                    ;
            int resultSet = this.dboConnect.createStatement().executeUpdate(statement);
            if (resultSet == 0){
                return "Problemas ao inserir o elemento.";
            }
            return null;
        } catch (SQLException ex) {
            Logger.getLogger(DBODisciplina.class.getName()).log(Level.SEVERE, null, ex);
            return "Problemas com a conexão com a base de dados.";
        }
    }
    
    /**
     * Exclui um elemento.
     * @param disciplina
     * @return 
     */
    public String delete(Disciplina disciplina){
        try {           
            String statement = "DELETE FROM disciplinas WHERE "
                    + "nome = '" + disciplina.getNome() + "'" + " AND "
                    + "cargaHoraria = " + disciplina.getCargaHoraria() + " AND "
                    + "vagas = " + disciplina.getVagas() + " AND "
                    + "cursos_id = " + disciplina.getCurso().getId() + " AND "
                    + "turnos_id = " + disciplina.getTurno().getId()
                    ;
            int resultSet = this.dboConnect.createStatement().executeUpdate(statement);
            if (resultSet == 0){
                return "Problemas ao inserir o elemento.";
            }
            return null;
        } catch (SQLException ex) {
            Logger.getLogger(DBODisciplina.class.getName()).log(Level.SEVERE, null, ex);
            return "Problemas com a conexão com a base de dados.";
        }
    }
    
    /**
     * Seleciona todos os elementos.
     * @return 
     */
    public List<Disciplina> all(){
        try {
            List<Disciplina> disciplinas = new ArrayList();
            String statement = "SELECT * FROM disciplinas";
            ResultSet resultSet = this.dboConnect.createStatement().executeQuery(statement);
            while (resultSet.next()){
                Disciplina disciplina = new Disciplina(
                        resultSet.getInt("id"), 
                        resultSet.getString("nome"),
                        resultSet.getInt("cargahoraria"), 
                        resultSet.getInt("vagas"),
                        new DBOCurso().selectWhereId(resultSet.getInt("cursos_id")),
                        new DBOTurno().selectWhereId(resultSet.getInt("turnos_id"))
                );
                disciplinas.add(disciplina);
            }
            return disciplinas;
        } catch (SQLException ex) {
            Logger.getLogger(DBODisciplina.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }
}
