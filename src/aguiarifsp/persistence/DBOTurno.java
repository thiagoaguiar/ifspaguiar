//******************************************************
//Instituto Federal de São Paulo - Campus Sertãozinho
//Disciplina......: M3LPBD
//Programação de Computadores e Dispositivos Móveis
//Aluno...........: Thiago Vieira de Aguiar
//******************************************************
package aguiarifsp.persistence;

import aguiarifsp.models.Turno;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author aguiar
 */
public class DBOTurno {
    
    private Connection dboConnect;
    
    public DBOTurno() {
        this.dboConnect = DBOConnect.getInstance().getConnection();
    }
    
    /**
     * Seleciona um elemento pelo ID.
     * @param id
     * @return 
     */
    public Turno selectWhereId(int id){
        try {
            Turno turno = null;
            String statement = "SELECT * FROM turnos WHERE id = "+id;
            ResultSet resultSet = this.dboConnect.createStatement().executeQuery(statement);
            if (resultSet.next()){
                turno = new Turno(
                        resultSet.getInt("id"),
                        resultSet.getString("nome"));
                return turno;
            } else {
                return null;
            }
        } catch (SQLException ex) {
            Logger.getLogger(DBOTurno.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }
    
    /**
     * Seleciona um elemento pelo campo.
     * @param campo
     * @param valor
     * @return 
     */
    public Turno selectWhereString(String campo, String valor){
        try {
            Turno turno = null;
            String statement = "SELECT * FROM turnos WHERE "+ campo +" = '"+ valor +"'";
            ResultSet resultSet = this.dboConnect.createStatement().executeQuery(statement);
            if (resultSet.next()){
                turno = new Turno(
                        resultSet.getInt("id"),
                        resultSet.getString("nome"));
                return turno;
            } else {
                return null;
            }
        } catch (SQLException ex) {
            Logger.getLogger(DBOTurno.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }
    
    /**
     * Seleciona todos os elemento.
     * @return 
     */
    public List<Turno> all(){
        try {
            List<Turno> turnos = new ArrayList();
            String statement = "SELECT * FROM turnos";
            ResultSet resultSet = this.dboConnect.createStatement().executeQuery(statement);
            while (resultSet.next()){
                Turno turno = new Turno(
                        resultSet.getInt("id"), 
                        resultSet.getString("nome"));
                turnos.add(turno);
            }
            return turnos;
        } catch (SQLException ex) {
            Logger.getLogger(DBOTurno.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }
    
    /**
     * Insere um elemento.
     * @param nome
     * @return 
     */    
    public String insert(String nome){
        
        try {            
            String statement = "INSERT INTO turnos (nome) VALUES "
                    + "("
                    + "'" + nome + "'" + ")"
                    ;
            int resultSet = this.dboConnect.createStatement().executeUpdate(statement);
            if (resultSet == 0){
                return "Problemas ao inserir o elemento.";
            }
            return null;
        } catch (SQLException ex) {
            Logger.getLogger(DBODisciplina.class.getName()).log(Level.SEVERE, null, ex);
            return "Problemas com a conexão com a base de dados.";
        }
    }
    
}
