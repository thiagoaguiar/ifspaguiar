//******************************************************
//Instituto Federal de São Paulo - Campus Sertãozinho
//Disciplina......: M3LPBD
//Programação de Computadores e Dispositivos Móveis
//Aluno...........: Thiago Vieira de Aguiar
//******************************************************
package aguiarifsp.persistence;

import aguiarifsp.models.Curso;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author aguiar
 */
public class DBOCurso {
    
    private Connection dboConnect;
    
    public DBOCurso() {
        this.dboConnect = DBOConnect.getInstance().getConnection();
    }
    
    /**
     * Seleciona um elemento pelo ID.
     * @param id
     * @return 
     */
    public Curso selectWhereId(int id){
        try {
            Curso curso = null;
            String statement = "SELECT * FROM cursos WHERE id = "+id;
            ResultSet resultSet = this.dboConnect.createStatement().executeQuery(statement);
            if (resultSet.next()){
                curso = new Curso(
                        resultSet.getInt("id"), 
                        resultSet.getString("nome"));
                return curso;
            } else {
                return null;
            }
        } catch (SQLException ex) {
            Logger.getLogger(DBOCurso.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }
  
    /**
     * Seleciona um elemento pelo campo.
     * @param campo
     * @param valor
     * @return 
     */
    public Curso selectWhereString(String campo, String valor){
        try {
            Curso curso = null;
            String statement = "SELECT * FROM cursos WHERE "+ campo +" = '"+ valor + "'";
            ResultSet resultSet = this.dboConnect.createStatement().executeQuery(statement);
            if (resultSet.next()){
                curso = new Curso(
                        resultSet.getInt("id"), 
                        resultSet.getString("nome"));
                return curso;
            } else {
                return null;
            }
        } catch (SQLException ex) {
            Logger.getLogger(DBOCurso.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }
    
    /**
     * Seleciona todos os elemento.
     * @return 
     */
    public List<Curso> all(){
        try {
            List<Curso> cursos = new ArrayList();
            String statement = "SELECT * FROM cursos";
            ResultSet resultSet = this.dboConnect.createStatement().executeQuery(statement);
            while (resultSet.next()){
                Curso curso = new Curso(
                        resultSet.getInt("id"), 
                        resultSet.getString("nome"));
                cursos.add(curso);
            }
            return cursos;
        } catch (SQLException ex) {
            Logger.getLogger(DBOCurso.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }
    
    /**
     * Insere um elemento.
     * @param nome
     * @return 
     */
    public String insert(String nome){
        
        try {            
            String statement = "INSERT INTO cursos (nome) VALUES "
                    + "("
                    + "'" + nome + "'" + ")"
                    ;
            int resultSet = this.dboConnect.createStatement().executeUpdate(statement);
            if (resultSet == 0){
                return "Problemas ao inserir o elemento.";
            }
            return null;
        } catch (SQLException ex) {
            Logger.getLogger(DBODisciplina.class.getName()).log(Level.SEVERE, null, ex);
            return "Problemas com a conexão com a base de dados.";
        }
    }
    
}
