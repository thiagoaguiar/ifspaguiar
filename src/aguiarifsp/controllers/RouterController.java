//******************************************************
//Instituto Federal de São Paulo - Campus Sertãozinho
//Disciplina......: M3LPBD
//Programação de Computadores e Dispositivos Móveis
//Aluno...........: Thiago Vieira de Aguiar
//******************************************************
package aguiarifsp.controllers;

import javax.swing.JFrame;

/**
 *
 * @author aguiar
 */
public class RouterController {
    
    /**
     * Gerencia a navegação entre os JFrames.
     * @param origem
     * @param destino 
     */
    public void routering(JFrame origem, JFrame destino){
        origem.setVisible(false);
        destino.setLocationRelativeTo(null);
        destino.setVisible(true);
    }
    
}
