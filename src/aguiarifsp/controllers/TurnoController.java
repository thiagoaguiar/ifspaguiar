//******************************************************
//Instituto Federal de São Paulo - Campus Sertãozinho
//Disciplina......: M3LPBD
//Programação de Computadores e Dispositivos Móveis
//Aluno...........: Thiago Vieira de Aguiar
//******************************************************
package aguiarifsp.controllers;

import aguiarifsp.models.Turno;
import aguiarifsp.persistence.DBOTurno;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author aguiar
 */
public class TurnoController {
    private static TurnoController instance;
    
    private List<Turno> turnos = new ArrayList();
    
    private TurnoController(){
            DBOTurno dboTurno = new DBOTurno();
            this.turnos = dboTurno.all();
    }
    
    /**
     * Retorna a instância do CursoController.
     * @return TurnoController
     */
    public static TurnoController getInstance(){
        if (instance == null){
            instance = new TurnoController();
        }
        return instance;
    }

    /**
     * Retorna uma lista de cursos cadastrados na base de dados.
     * @return List<Turno>
     */
    public List<Turno> getTurnos() {
        return turnos;
    }
    
    /**
     * Adiciona elementos na tabela da interface.
     * @param jTable
     * @param curso 
     */
    public void addElementoTabela(JTable jTable, Turno curso){
        Vector v = new Vector();
        v.add(curso.getId());
        v.add(curso.getNome());
        ((DefaultTableModel)jTable.getModel()).addRow(v);
    }

}
