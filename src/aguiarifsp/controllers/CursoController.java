//******************************************************
//Instituto Federal de São Paulo - Campus Sertãozinho
//Disciplina......: M3LPBD
//Programação de Computadores e Dispositivos Móveis
//Aluno...........: Thiago Vieira de Aguiar
//******************************************************
package aguiarifsp.controllers;

import aguiarifsp.models.Curso;
import aguiarifsp.persistence.DBOCurso;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author aguiar
 */
public class CursoController {
    private static CursoController instance;

    private List<Curso> cursos = new ArrayList();

    private CursoController(){
        DBOCurso dboCurso = new DBOCurso();
        this.cursos = dboCurso.all();
    }

    /**
     * Retorna a instância do CursoController.
     * @return CursoController
     */
    public static CursoController getInstance(){
        if (instance == null){
            instance = new CursoController();
        }
        return instance;
    }

    /**
     * Retorna uma lista de cursos cadastrados na base de dados.
     * @return List<Curso>
     */
    public List<Curso> getCursos() {
        return cursos;
    }

    /**
     * Adiciona elementos na tabela da interface.
     * @param jTable
     * @param curso 
     */
    public void addElementoTabela(JTable jTable, Curso curso){
        Vector v = new Vector();
        v.add(curso.getId());
        v.add(curso.getNome());
        ((DefaultTableModel)jTable.getModel()).addRow(v);
    }
        
        
}
