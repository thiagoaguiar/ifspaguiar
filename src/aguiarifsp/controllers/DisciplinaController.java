//******************************************************
//Instituto Federal de São Paulo - Campus Sertãozinho
//Disciplina......: M3LPBD
//Programação de Computadores e Dispositivos Móveis
//Aluno...........: Thiago Vieira de Aguiar
//******************************************************
package aguiarifsp.controllers;

import aguiarifsp.models.Disciplina;
import aguiarifsp.persistence.DBODisciplina;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author aguiar
 */
public class DisciplinaController {
    private static DisciplinaController instance;
    
    private List<Disciplina> disciplinas = new ArrayList();
    
    private DisciplinaController(){
    }
    
    /**
     * Retorna a instância do CursoController.
     * @return DisciplinaController
     */    
    public static DisciplinaController getInstance(){
        if (instance == null){
            instance = new DisciplinaController();
        }
        return instance;
    }

    /**
     * Retorna uma lista de cursos cadastrados na base de dados.
     * @return List<Disciplina>
     */
    public List<Disciplina> getDisciplinas() {
        this.disciplinas = new DBODisciplina().all();
        return disciplinas;
    }
    
    /**
     * Adiciona elementos na tabela da interface.
     * @param jTable
     * @param curso 
     */
    public void addDisciplinaTabela(JTable jTable, Disciplina disciplina){
        Vector v = new Vector();
        v.add(disciplina.getId());
        v.add(disciplina.getNome());
        v.add(disciplina.getCargaHoraria());
        v.add(disciplina.getVagas());
        v.add(disciplina.getCurso().getNome());
        v.add(disciplina.getTurno().getNome());
        ((DefaultTableModel)jTable.getModel()).addRow(v);
    }

}
